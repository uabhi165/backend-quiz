const mongoose = require('mongoose');
const Question=require("./models/questionModel")
const Test=require("./models/testModel")
const Attempt=require("./models/attemptModel")
const Subject=require("./models/subjectModel")
const express = require("express");

const cors = require('cors');
const app = express();
const port = 3000;
app.use(cors())
// app.use(express.json())

main().catch(err => console.log(err));

// app.use(
//   express.urlencoded({ extended: true })
// );
  
app.use(express.json());



async function main() {
    await mongoose.connect('mongodb+srv://admin:wgQb6l9sHDill4ga@cluster0.chawg2p.mongodb.net/?retryWrites=true&w=majority');
}

app.post("/questions", async(req,res)=>{
    const question = new Question(req.body);
    await question.save()
    res.json(question).status(201)

})
app.post("/tests",async(req,res)=>{                       
  const test=new Test(req.body);
  await test.save()
  res.json(test).status(201)
})

app.get("/questions", async function (req, res) {
 const questions=await Question.find({});
 res.json(questions).status(200)
});

app.post("/attempt",async(req,res)=>{                       
  const attempt=new Attempt(req.body);
  await attempt.save()
  res.json(attempt).status(201)
})

 
app.post("/subjects", async function (req, res) {
  const subjects=new Subject(req.body);
  await subjects.save()
  res.json(subjects).status(201)
});
 


app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});


