const mongoose = require('mongoose');
const attemptSchema = new mongoose.Schema({

 questions:[{
    question:{
        type:mongoose.Types.ObjectId,
        ref:`Question`,
        required:true
        
    },
    selections:{
        type:String,
        // required:true
    }
 }]
});

const Attempt= mongoose.model('Attempt', attemptSchema);

module.exports=Attempt