const mongoose = require('mongoose');
const testSchema = new mongoose.Schema({
    name: String,
    questions: [
        {
            type: mongoose.Types.ObjectId,
            ref:'Question'
        }
    ],
    
});

const Test= mongoose.model('Test', testSchema);

module.exports=Test